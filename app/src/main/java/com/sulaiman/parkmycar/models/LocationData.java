package com.sulaiman.parkmycar.models;

/**
 * Created by sulaiman on 12/17/17.
 */

public class LocationData {
    private String email;
    private String name;
    private double lattitude, longitude;
    private String address;

    public LocationData() {

    }

    public LocationData(String email, String name, double lattitude, double longitude, String address) {
        this.email = email;
        this.name = name;
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
