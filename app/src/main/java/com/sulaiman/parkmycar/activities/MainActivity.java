package com.sulaiman.parkmycar.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sulaiman.parkmycar.R;
import com.sulaiman.parkmycar.models.LocationData;
import com.sulaiman.parkmycar.utility.DialogProvider;
import com.sulaiman.parkmycar.utility.Logger;
import com.sulaiman.parkmycar.utility.PermissionManager;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, FirebaseAuth.AuthStateListener, ChildEventListener,
        GoogleMap.OnMapLongClickListener, NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "MainActivity";

    //My code
    private FirebaseDatabase mFrebaseDatabase;
    private DatabaseReference mUserDatabaseReference;

    //Authentication
    private FirebaseAuth mFirebaseAuth;
    //Database
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mChatPhotosStorageReference;

    //Google Map

    private GoogleMap googleMap;
    private LocationManager locationManager;


    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    final static int RC_SIGN_IN = 1001;

    private Button gpsOnOffButton;

    //Test codes for locations
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastSavedLocation;


    //Dummy use only

    Toolbar toolbar;
    FloatingActionButton fab;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Logger.debugLog(TAG, "onCreate");
        setupContents();



       /* gpsOnOffButton = findViewById(R.id.gps_button);

        setUpFirebase();

        SupportMapFragment mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        mSupportMapFragment.getMapAsync(this);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();


        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);*/


//        gpsOnOffButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Logger.debugLog(TAG, "onClick ");
////                if (isMyGpsOn()) {
////                    turnOfGPS();
////                } else {
////                    turnOnGPS();
////                }
//            }
//        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.debugLog(TAG, "onPause");
        if (mFirebaseAuth != null)
            mFirebaseAuth.removeAuthStateListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.debugLog(TAG, "onResume");
        if (mFirebaseAuth != null)
            mFirebaseAuth.addAuthStateListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupContents() {
        Logger.debugLog(TAG, "setupContents");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = findViewById(R.id.floating_action_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Clicked on floating Action button", 200).setAction("Action", null).show();
            }
        });

        drawerLayout = findViewById(R.id.drawer_layout);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_nav_drawer, R.string.close_nav_drawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void setUpFirebase() {
        mFrebaseDatabase = FirebaseDatabase.getInstance();
        mUserDatabaseReference = mFrebaseDatabase.getReference().child("users");

        mFirebaseAuth = FirebaseAuth.getInstance();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.debugLog(TAG, "onActivityResult");
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
                        Toast.makeText(MainActivity.this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(MainActivity.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;

            case RC_SIGN_IN:
                if (requestCode == RC_SIGN_IN) {
                    if (resultCode == RESULT_OK) {
                        Toast.makeText(this, "Sign is successful", Toast.LENGTH_SHORT).show();
                    } else if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(this, "Sign is un-successful", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Logger.debugLog(TAG, "onMapReady");
        this.googleMap = googleMap;

        setUpMap();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionManager.REQUEST_CODE_LOCATION:
                if (grantResults != null && grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        try {
                            this.googleMap.setMyLocationEnabled(true);
                        } catch (SecurityException e) {
                            Logger.errorLog(TAG, "onRequestPermissionsResult " + e.toString());
                        }

                    }
                }
        }
    }

    private void setUpMap() {
        this.googleMap.setMinZoomPreference(10);
        this.googleMap.setMaxZoomPreference(20);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setTrafficEnabled(true);
        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        googleMap.setOnMapLongClickListener(this);

        if (PermissionManager.check_FINE_LOCATION_Permission(this) && PermissionManager.check_ACCESS_COARSE_LOCATION(this)) {
            googleMap.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    PermissionManager.REQUEST_CODE_LOCATION);
        }
    }

    private boolean isMyGpsOn() {
        if (locationManager == null) {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        }
        if (PermissionManager.check_ACCESS_COARSE_LOCATION(this) && PermissionManager.check_FINE_LOCATION_Permission(this)) {
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } else {
            return false;
        }
    }

    private void setMarkerOnMyLastKnownLocation(Location location) {
        Logger.debugLog(TAG, "setMarkerOnMyLastKnownLocation " + location);
        mLastSavedLocation = location;
        LatLng lastKnownLocation = new LatLng(location.getLatitude(), location.getLongitude());


        this.googleMap.addMarker(new MarkerOptions().position(lastKnownLocation).title("My last known location"));
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastKnownLocation, 18));
//        this.googleMap.animateCamera(CameraUpdateFactory.zoomBy(10));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Logger.debugLog(TAG, "onConnected");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                Logger.debugLog(TAG, "onResult i: " + result.toString());
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        Logger.debugLog(TAG, "onResult SUCCESS");

                        if (PermissionManager.check_ACCESS_COARSE_LOCATION(MainActivity.this) && PermissionManager.check_FINE_LOCATION_Permission(MainActivity.this)) {
                            mFusedLocationProviderClient.getLastLocation()
                                    .addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                                        @Override
                                        public void onSuccess(Location location) {
                                            Logger.debugLog(TAG, "getLastLocation onSuccess");
                                            if (location != null)
                                                setMarkerOnMyLastKnownLocation(location);
                                        }
                                    });
                        }

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        Logger.debugLog(TAG, "onResult RESOLUTION_REQUIRED");
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MainActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        Logger.debugLog(TAG, "onResult SETTINGS_CHANGE_UNAVAILABLE");

                        break;
                }
            }
        });

    }

    @Override
    public void onConnectionSuspended(int i) {
        Logger.debugLog(TAG, "onConnectionSuspended i: " + i);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Logger.debugLog(TAG, "onConnectionFailed " + connectionResult.getErrorMessage());
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        Logger.debugLog(TAG, "onAuthStateChanged");

        FirebaseUser user = mFirebaseAuth.getCurrentUser();
        if (user == null) {
            startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setIsSmartLockEnabled(false)
                    .setAvailableProviders(Arrays.asList(
                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                            new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build()
                            )
                    ).build(), 1001);
            mUserDatabaseReference.removeEventListener(this);
        } else {
            mUserDatabaseReference.addChildEventListener(this);
            if (mLastSavedLocation != null) {
                LocationData locationData = new LocationData();
                locationData.setEmail(user.getEmail());
                locationData.setName(user.getDisplayName());
                locationData.setLattitude(mLastSavedLocation.getLatitude());
                locationData.setLongitude(mLastSavedLocation.getLongitude());
                mUserDatabaseReference.push().setValue(locationData);
            }

        }

    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Logger.debugLog(TAG, "onChildAdded");
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        Logger.debugLog(TAG, "onChildChanged");
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        Logger.debugLog(TAG, "onChildRemoved");
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        Logger.debugLog(TAG, "onChildMoved");
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Logger.debugLog(TAG, "onCancelled");
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        Logger.debugLog(TAG, "onMapLongClick " + latLng);
        DialogProvider.showAddressInputDialog(this, latLng).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Logger.debugLog(TAG, "onNavigationItemSelected");
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
