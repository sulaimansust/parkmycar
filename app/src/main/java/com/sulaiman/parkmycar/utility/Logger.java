package com.sulaiman.parkmycar.utility;

import android.util.Log;

/**
 * Created by sulaimankhan on 12/16/2017.
 */

public class Logger {
    private static boolean isDebugOn = true;

    public static void debugLog(String TAG, String message) {
        if (isDebugOn)
            Log.d(TAG, "--> " + message);
    }

    public static void errorLog(String TAG, String message) {
        if (isDebugOn)
            Log.e(TAG, "--> " + message);
    }

}
