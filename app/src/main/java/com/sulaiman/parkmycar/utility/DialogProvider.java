package com.sulaiman.parkmycar.utility;

import android.app.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sulaiman.parkmycar.R;
import com.sulaiman.parkmycar.firebasedb.FirebaseDatabaseHandler;
import com.sulaiman.parkmycar.models.LocationData;

/**
 * Created by sulaiman on 12/18/17.
 */

public class DialogProvider {
    public static final String TAG = "DialogProvider";

    static AlertDialog showAlertDialog = null;

    public static Dialog showAddressInputDialog(Activity activity, final LatLng latLng) {
        Logger.debugLog(TAG, "showAddressInputDialog");

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        View customView = activity.getLayoutInflater().inflate(R.layout.address_input_dialog, null);
        final EditText addressText = customView.findViewById(R.id.address_input_text);
        builder.setView(customView).setTitle("Insert your address")
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Logger.debugLog(TAG, "showAddressInputDialog onClick positive");
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        LocationData locationData = new LocationData(user.getEmail(), user.getDisplayName(), latLng.latitude, latLng.longitude, addressText.getText().toString());
                        FirebaseDatabaseHandler.getInstance().pushOwnerData(locationData);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Logger.debugLog(TAG, "showAddressInputDialog onClick negative");
                        dialogInterface.dismiss();
                    }
                });
        showAlertDialog = builder.create();
        return showAlertDialog;

    }
}
