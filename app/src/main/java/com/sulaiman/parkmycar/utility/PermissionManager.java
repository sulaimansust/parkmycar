package com.sulaiman.parkmycar.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;

/**
 * Created by sulaiman on 10/19/2017.
 */

public class PermissionManager {

    public static final String TAG = "PermissionManager";

    public static final int REQUEST_CODE = 0;
    public static final int REQUEST_CODE_CAMERA = 1;
    public static final int REQUEST_CODE_CONTACTS_READ = 2;
    public static final int REQUEST_CODE_SMS_READ = 3;
    public static final int REQUEST_CODE_LOCATION = 4;


    public static boolean checkPermission(Activity activity, String PERMISSION_NAME) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR1)
            return true;
        try {
            int result = ContextCompat.checkSelfPermission(activity, PERMISSION_NAME);
            if (result == PackageManager.PERMISSION_GRANTED)
                return true;
            else
                return false;

        } catch (Exception e) {
            Logger.errorLog(TAG, e.toString());
            return false;
        }

    }


    public static boolean check_PHONE_STATE_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        try {
            int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //  ActivityCompat.requestPermissions(activity, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_CAMERA);
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }

    // for checking accurate locatio
    public static boolean check_FINE_LOCATION_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        try {
            int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //  ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_ACCESS_FINE_LOCATION);
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }

    //for checking approximate location
    public static boolean check_ACCESS_COARSE_LOCATION(Context activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        try {
            int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //  ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_ACCESS_FINE_LOCATION);
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }

    public static boolean check_CAMERA_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        try {
            int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //  ActivityCompat.requestPermissions(activity, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_CAMERA);
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }

    public static boolean check_READ_CONTACTS_Permission(Context context) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        try {
            int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //   ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_READ_PHONE_STATE);
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }


    public static boolean check_READ_CONTACTS_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        try {

            int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //   ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_READ_PHONE_STATE);
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }

    public static boolean check_READ_PHONE_STATE_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            //    ActivityCompat.requestPermissions(activity, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_READ_PHONE_STATE);
            return false;
        }
    }


    public static boolean check_READ_EXTERNAL_STORAGE_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        try {
            int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //  ActivityCompat.requestPermissions(activity, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_WRITE_EXTERNAL_STORAGE);
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }


    public static boolean check_WRITE_SETTINGS_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            return true;
        } else {
            if (Settings.System.canWrite(activity)) {
                int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_SETTINGS);
                if (result == PackageManager.PERMISSION_GRANTED) {
                    return true;
                } else {
                    //  ActivityCompat.requestPermissions(activity, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_WRITE_EXTERNAL_STORAGE);
                    return false;
                }

            } else {
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                return true;
            }
        }

    }


    public static boolean check_RECORD_AUDIO_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
            return true;
        try {
            int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //  ActivityCompat.requestPermissions(activity, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION}, requestCode_WRITE_EXTERNAL_STORAGE);
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }

    public static boolean check_READ_SMS_Permission(Activity activity) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            return true;
        }
        try {

            int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_SMS);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } catch (RuntimeException exceptionIgnored) {
            return false;
        }
    }
}
