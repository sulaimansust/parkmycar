package com.sulaiman.parkmycar.firebasedb;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sulaiman.parkmycar.models.LocationData;
import com.sulaiman.parkmycar.utility.Logger;

/**
 * Created by sulaiman on 12/18/17.
 */

public class FirebaseDatabaseHandler implements ChildEventListener {
    public static final String TAG = "FirebaseDatabaseHandler";


    private static FirebaseDatabaseHandler mFirebaseDatabaseHandler;

    public FirebaseDatabase mFirebaseDatabase;
    public DatabaseReference mDatabaseReference;

    private FirebaseDatabaseHandler() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference().child("owners");
        mDatabaseReference.addChildEventListener(this);
    }

    public static FirebaseDatabaseHandler getInstance() {
        if (mFirebaseDatabaseHandler == null) {
            mFirebaseDatabaseHandler = new FirebaseDatabaseHandler();
        }
        return mFirebaseDatabaseHandler;
    }

    public void pushOwnerData(LocationData data) {
        mDatabaseReference.push().setValue(data);
    }


    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Logger.debugLog(TAG, "onChildAdded");
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        Logger.debugLog(TAG, "onChildChanged");
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        Logger.debugLog(TAG, "onChildRemoved");
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        Logger.debugLog(TAG, "onChildMoved");
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Logger.debugLog(TAG, "onCancelled");
    }

}
